package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.OutOfStockException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;
import net.stawrul.services.BooksService;
import net.stawrul.services.exceptions.EmptyException;
import net.stawrul.services.exceptions.SizeException;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest {

    @Mock
    EntityManager em;

    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(0);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected
    }

    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne() {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

    @Test
    public void whenGivenLowercaseString_toUpperReturnsUppercase() {

        //Arrange
        String lower = "abcdef";

        //Act
        String result = lower.toUpperCase();

        //Assert
        assertEquals("ABCDEF", result);
    }
   
    @Test(expected = EmptyException.class)
    public void validateEmptyCollection() {
        // Arrange
        Order order = new Order();
        Book book = new Book();
        OrdersService orderService = new OrdersService(em);

        //Act
        orderService.placeOrder(order);

        //Assert - exception expected
    }
    @Test(expected = SizeException.class)
    public void validateCollectionSize() {
        // Arrange
        Order order = new Order();
        Book book = new Book();
        Book book1 = new Book();
        Book book2 = new Book();
        book.setAmount(1);
        book1.setAmount(1);
        book2.setAmount(1);
        order.getBooks().add(book);
        order.getBooks().add(book1);
        order.getBooks().add(book2);
        
        OrdersService orderService = new OrdersService(em);


        //Act
        orderService.placeOrder(order);

        //Assert - exception expected
    }
}
